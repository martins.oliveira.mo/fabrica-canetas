public class Caneta{

	private String cor;
	private String material;
	private Boolean tampa;

	public void setCor(String umaCor){
		cor=umaCor;	
	}
	
	public String getCor(){
		return cor;
	}

	public void setMateral(String tipoMaterial){
		material=tipoMaterial;
	}

	public void setTampa (Boolean temTampa){
		tampa=temTampa;
	}

	public void mostra(){
		System.out.println("Cor: "+cor);
		System.out.println("material: "+material);
		System.out.println("tampa :"+tampa);
	}
}

