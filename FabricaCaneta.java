import java.util.Scanner; 
public class FabricaCaneta{
	
	public static void main(String []args) {
		Scanner ler = new Scanner(System.in); 
		
		Caneta umaCaneta;
		umaCaneta = new Caneta();
		
		System.out.println("Qual a cor da caneta?");
		umaCaneta.setCor(ler.nextLine());
		System.out.println("Caneta "+umaCaneta.getCor()+" foi criada com sucesso!");
		
		System.out.println("Qual o material da caneta?");
		String material = ler.nextLine();
		umaCaneta.setMateral(material);
	
		System.out.println("Tem tampa? true(sim) |false(não) ");
		Boolean tampa = ler.nextBoolean();
		umaCaneta.setTampa(tampa);
		
		System.out.println("\n\n");
		umaCaneta.mostra();
	}
}

